#!/bin/bash

true \
&& docker-compose down \
&& mvn -Drat.skip=true package \
&& echo "!SUCCESS BUILD" \
&& exit 0

echo "!ERROR BUILD"
&& exit 1


