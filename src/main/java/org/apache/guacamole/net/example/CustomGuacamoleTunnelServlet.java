package org.apache.guacamole.net.example;

import javax.servlet.http.HttpServletRequest;
import org.apache.guacamole.GuacamoleException;
import org.apache.guacamole.net.GuacamoleSocket;
import org.apache.guacamole.net.GuacamoleTunnel;
import org.apache.guacamole.net.InetGuacamoleSocket;
import org.apache.guacamole.net.SimpleGuacamoleTunnel;
import org.apache.guacamole.protocol.ConfiguredGuacamoleSocket;
import org.apache.guacamole.protocol.GuacamoleConfiguration;
import org.apache.guacamole.servlet.GuacamoleHTTPTunnelServlet;

public class CustomGuacamoleTunnelServlet
    extends GuacamoleHTTPTunnelServlet {

    @Override
    protected GuacamoleTunnel doConnect(HttpServletRequest request)
        throws GuacamoleException {

        // Validate request params
        String protocol = "";
        String port = "";
        String hostname = "";

        if (!request.getParameterMap().containsKey("protocol")) {
            throw new GuacamoleException("Must have protocol (ssh, rdp, vnc)");
        }
        if (!request.getParameterMap().containsKey("hostname")) {
            throw new GuacamoleException("Must have hostname");
        }
        protocol = request.getParameter("protocol");
        hostname = request.getParameter("hostname");
        if (!request.getParameterMap().containsKey("port")) {
            switch (protocol) {
                case "ssh":
                    port = "22";
                    break;

                case "rdp":
                    port = "3389";
                    break;

                case "vnc":
                    port = "5900";
                    break;
            }
        }
        // Create config from query string
        GuacamoleConfiguration config = new GuacamoleConfiguration();
        config.setProtocol(protocol);
        config.setParameter("hostname", hostname);
        config.setParameter("port", port);
        

        // Connect to guacd - everything is hard-coded here.
        GuacamoleSocket socket = new ConfiguredGuacamoleSocket(
                new InetGuacamoleSocket("guacd", 4822),
                config
        );

        // Return a new tunnel which uses the connected socket
        return new SimpleGuacamoleTunnel(socket);

    }

}